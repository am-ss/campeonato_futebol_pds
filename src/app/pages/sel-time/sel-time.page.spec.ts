import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelTimePage } from './sel-time.page';

describe('SelTimePage', () => {
  let component: SelTimePage;
  let fixture: ComponentFixture<SelTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelTimePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
